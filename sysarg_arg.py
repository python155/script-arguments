# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 08:26:58 2021

@author: Brianna

Title: Sys_Arg Script
Purpose: Script uses sys_arg module to receive in arguments passed
         - Bare minimum features, only use if really needed
"""
# USAGE
# Terminal: python3 sysarg_arg.py one two three 5
# IPython: %run ./sysarg_arg one two three 5
# Debugger: Click on Run --> Configuration Per File --> General Settings --> Command Line options

import sys
 
# total arguments
n = len(sys.argv)
print("Total arguments passed:", n)
 
# Arguments passed
print("\nName of Python script:", sys.argv[0])
 
print("\nArguments passed:", end = " ")
for i in range(1, n):
    print(sys.argv[i], end = " ")
     
