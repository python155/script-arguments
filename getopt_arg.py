# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 08:26:58 2021

@author: Brianna

Title: getopt_arg
Purpose: Python Script to run arg commands using getopt module  was stripped. 
"""
# Common syntax of receiving arguments:
#     Syntax: getopt.getopt(args, options, [long_options])
#     Parameters: 
#         args: List of arguments to be passed. 
#         options: String of option letters that the script want to recognize.
#                      NOTE: Options that require an argument should be followed by a colon (:). 
#         long_options: List of string with the name of long options. 
#                           NOTE: Options that require arguments should be followed by an equal sign (=).
#         Return Type: Returns value consisting of two elements: 
#                      1) The first is a list of (option, value) pairs. 
#                      2) The second is the list of program arguments left after the option list  


# USAGE
# Terminal: python3 getopt.py -h -i <input_file_path> -o <output_file_path>
# IPython: %run ./getopt -h -i <input_file_path> -o <output_file_path>
# Debugger: Click on Run --> Configuration Per File --> General Settings --> Command Line options


import getopt, sys


# Remove 1st argument from the
# list of command line arguments
argumentList = sys.argv[1:]

# Options
options = "hi:o:"

# Long options
long_options = ["Help", "In_file =", "Output ="]

try:
	# Parsing argument
	arguments, values = getopt.getopt(argumentList, options, long_options)
	
	# checking each argument
	for currentArgument, currentValue in arguments:

		if currentArgument in ("-h", "--Help"):
			print ("Usage: python3 getopt_arg.py -i <input_file_name> -o <output_file_name>")
			
		elif currentArgument in ("-i", "--In_file"):
			print ("Displaying file_name: {}".format(currentValue))
			
		elif currentArgument in ("-o", "--Output"):
			print ("Enabling special output mode {}".format(currentValue))
			
except getopt.error as err:
	# output error, and return with an error code
	print (str(err))
