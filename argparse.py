# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 08:44:34 2021

@author: Brianna
"""

# Python program to demonstrate
# command line arguments
 
# USAGE
# Terminal: python3 argparse.py -h -i input.csv -o output.csv
# IPython: %run ./argparse -h -i input.csv -o output.csv
# Debugger: Click on Run --> Configuration Per File --> General Settings --> Command Line options


import argparse
 
msg = "Adding description and usage details"
 
# Initialize parser
parser = argparse.ArgumentParser(description = msg)
#parser.parse_args()

# Adding optional argument
parser.add_argument("-o", "--Output", help = "Show Output")
parser.add_argument("-i", "--Input", help = "Show Input")
 
# Read arguments from command line
args = parser.parse_args()
 
if args.Output:
    print("Diplaying Output as: {}".format(args.Output))
    
if args.Input:
    print("Displaying Input as: {}".format(args.Input))